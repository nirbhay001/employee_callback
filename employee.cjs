let data = require("./data.json");
let path = require('path');
let fs = require('fs');

function dataId(data, callbackFunction) {
    let pathFile = path.join(__dirname, "ids.json");
    let result = data.employees.filter((item) => {
        if (item.id === 2 || item.id === 13 || item.id === 23) {
            return item;
        }
    })
    fs.writeFile(pathFile, JSON.stringify(result), (error) => {
        if (error) {
            callbackFunction(err, null);
            return;
        }
        else {
            callbackFunction(null, data);
            return;
        }
        return;
    })
    return;
};

function groupData(data, callbackFunction) {
    let pathFile = path.join(__dirname, "groupCompany.json");
    let result = data.employees.reduce((prev, curr) => {
        if (prev[curr.company] === undefined) {
            prev[curr.company] = [];
            prev[curr.company].push(curr);
        }
        else {
            prev[curr.company].push(curr);
        }
        return prev;
    }, {})
    fs.writeFile(pathFile, JSON.stringify(result), (error) => {
        if (error) {
            callbackFunction(err, null);
            return;
        }
        else {
            callbackFunction(null, data);
            return;
        }
    })
    return;
}

function PowerpuffData(data, callbackFunction) {
    let pathFile = path.join(__dirname, "brigadeCompany.json");
    let result = data.employees.filter((item) => {
        return item.company === "Powerpuff Brigade";
    })
    fs.writeFile(pathFile, JSON.stringify(result), (error) => {
        if (error) {
            callbackFunction(err, null);
            return;
        }
        else {
            callbackFunction(null, data);
            return;
        }
    })
    return;
}

function entryId2(data, callbackFunction) {
    let pathFile = path.join(__dirname, "removeId2.json");
    let result = data.employees.filter((item) => {
        return item.id !== 2;
    })
    fs.writeFile(pathFile, JSON.stringify(result), (error) => {
        if (error) {
            callbackFunction(err, null);
            return;
        }
        else {
            callbackFunction(null, data);
            return;
        }
    })
    return;
}

function sortData(data, callbackFunction) {
    let pathFile = path.join(__dirname, "sortData.json");
    let result = data.employees.sort((item1, item2) => {
        if (item1.company < item2.company) {
            return -1;
        }
        else if (item1.company > item2.company) {
            return 1;
        }
        else {
            if (item1.id < item2.id) {
                return -1;
            }
            else if (item1.id > item2.id) {
                return 1;
            }
            else {
                return 0;
            }
        }
    })
    fs.writeFile(pathFile, JSON.stringify(result), (error) => {
        if (error) {
            callbackFunction(error, null);
            return;
        }
        else {
            callbackFunction(null, data);
            return;
        }
    })
    return;
}

function swapCompany(data, callbackFunction) {
    let pathFile = path.join(__dirname, "swapData.json");
    let result1 = data.employees.filter((item) => {
        return item.id === 92;
    })
    let result2 = data.employees.filter((item) => {
        return item.id === 93;
    })
    let newResult = data.employees.map((item) => {
        if (item.id === 92) {
            return result2;
        }
        else if (item.id === 93) {
            return result1;
        }
        return item;
    })
    fs.writeFile(pathFile, JSON.stringify(newResult), (error) => {
        if (error) {
            callbackFunction(error, null);
            return;
        }
        else {
            callbackFunction(null, data);
            return;
        }
    })
    return;
}

function birthdate(data, callbackFunction) {
    let pathFile = path.join(__dirname, "birthdayDate.json");
    let result = data.employees.map((item) => {
        if ((item.id) % 2 == 0) {
            item["birthday"] = new Date().getDate();
            return item;
        }
        return item;
    })
    fs.writeFile(pathFile, JSON.stringify(result), (error) => {
        if (error) {
            callbackFunction(error, null);
            return;
        }
        else {
            callbackFunction(null, "all file created succesfully");
            return;
        }
    })
    return;
}

dataId(data, (error, data) => {
    if (error) {
        console.log(error);
    }
    else {
        groupData(data, (error, data) => {
            if (error) {
                console.log(error);
            }
            else {
                PowerpuffData(data, (error, data) => {
                    if (error) {
                        console.log(error);
                    }
                    else {
                        entryId2(data, (error, data) => {
                            if (error) {
                                console.log(error);
                            }
                            else {
                                sortData(data, (error, data) => {
                                    if (error) {
                                        console.log(error);
                                    }
                                    else {
                                        swapCompany(data, (error, data) => {
                                            if (error) {
                                                console.log(error);
                                            }
                                            else {
                                                birthdate(data, (error, data) => {
                                                    if (error) {
                                                        console.log(error);
                                                    }
                                                    else {
                                                        console.log(data);
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    }
})
